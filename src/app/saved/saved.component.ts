import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { PostsService } from '../posts.service';
import { SavedService } from './../saved.service';
import { Posts } from '../interfaces/posts';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Component({
  selector: 'app-saved',
  templateUrl: './saved.component.html',
  styleUrls: ['./saved.component.css']
})
export class SavedComponent implements OnInit {

  constructor(private db:AngularFirestore, public authService:AuthService, private postsService:PostsService, public savedSecrvice:SavedService) { }

  userId:string;
  posts$

  public deleteSaved(id:string){
    this.savedSecrvice.deleteSaved(this.userId,id);
  }

  likes(likes,id){
    if(likes==null){
      likes=0
    };this.postsService.updateLikes(this.userId,id, likes+1);
  }

 ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.posts$ = this.savedSecrvice.getSaved(this.userId);
      }
    )
  }

    
}