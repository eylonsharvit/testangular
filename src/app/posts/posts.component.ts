import { AuthService } from './../auth.service';
import { Comments } from './../interfaces/comments';
import { Component, OnInit } from '@angular/core';
import { Posts } from '../interfaces/posts';
import { PostsService } from '../posts.service';
import { element } from 'protractor';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  postsdata$: Posts[]=[];
  commentsdata$: Comments[] =[];
  userId:string;
  result:string;
  message:string;
  hasError:boolean = false;

  constructor(private postsservice:PostsService, public authService:AuthService) { }
  
  addSaved(title:string ){
    this.postsservice.addSaved(this.userId, title);
    console.log('title:',title)
    this.result = "Saved!"
    
  }
  
  ngOnInit() {
    this.postsservice.getPosts().subscribe(data =>this.postsdata$ = data);
    this.postsservice.getComments().subscribe(data =>this.commentsdata$ = data);

    this.authService.getUser().subscribe(       
      user=>
      {
        this.userId = user.uid
        console.log('userID: ', this.userId);
      }
    );
  }
}