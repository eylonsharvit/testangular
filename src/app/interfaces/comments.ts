export interface Comments {
    postId:string,
    id: string,
    name:boolean,
    email: string,
    body: string
}
