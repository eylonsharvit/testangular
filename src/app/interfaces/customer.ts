export interface Customer {
    name: boolean;
    years: number;
    income: number;
    id?:string;
    saved?:Boolean;
    result?:string;     
}
