import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {
  url = 'https://uq0ck7pou3.execute-api.us-east-1.amazonaws.com/1'; 
  
  predict(years:number, income:number, name:boolean):Observable<any>{
    let json = {
      "data": 
        {
          "years": years,
          "income": income,
          "name":name
        }
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }

  
  constructor(private http: HttpClient) { }
}
